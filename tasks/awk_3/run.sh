#!/bin/bash

awk '{
average_mark = ($2 + $3 + $4) / 3
result = ""
if (average_mark >= 80) {
  result = "A"
} else {
  if (average_mark >= 60) {
    result = "B"
  } else {
    result = "FAIL"
  }
}
print $0 " : " result
}'
