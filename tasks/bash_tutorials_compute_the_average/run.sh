#!/bin/bash

read n
sum=0
for ((i = 0; i < $n; i++))
do
	read new_number
	sum=$(($sum+$new_number))
done

result=$(echo "scale=4;"$sum"/"$n"+0.0005" | bc)

echo ${result::-1}
