#!/bin/bash

awk '
{
  if (NR%3 == 0) {
    print first_line"\t"second_line"\t"$0
  } else if (NR%3 == 1) {
    first_line = $0
  } else {
    second_line = $0
  }
}

END {
  if (NR%3 == 1) print $0
  else if (NR%3 == 2) print first_line"\t"$0
}
'
