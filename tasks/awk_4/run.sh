#!/bin/bash

awk '
{
  if (NR%2 == 0) {
    print p ";" $0
  } else {
    p = $0
  }
}

END {
  if (NR%2) print p
}
'
