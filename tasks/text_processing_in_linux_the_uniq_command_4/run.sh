#!/bin/bash

uniq -c | cut -b 7- | awk '
{
  if ($1 == 1) print $2
}
' | head -c -2


